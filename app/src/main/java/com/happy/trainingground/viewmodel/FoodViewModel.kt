package com.happy.trainingground.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.happy.trainingground.repository.FoodRepository
import com.happy.trainingground.model.DataFoodItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class FoodViewModel(
    private val foodRepository: FoodRepository
) : ViewModel() {
    val todayFoodItemList: MutableLiveData<List<DataFoodItem>> by lazy { // LiveData
        MutableLiveData<List<DataFoodItem>>()
    }

    fun getTodayFood(context: Context, date: Date) {
        viewModelScope.launch(Dispatchers.Default) {
            val todayMenu = foodRepository.getFoodListByDate(context, date)
                .sortedBy {
                    it.price
                }
            todayFoodItemList.postValue(todayMenu)
        }
    }
}