package com.happy.trainingground.model


data class DataFoodItem(
    val id: String,
    val name: String,
    val displayImage: String,
    val ingredients: List<Ingredient>,
    val price: Double,
)

