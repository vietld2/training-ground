package com.happy.trainingground.model

data class Ingredient(val id: String, val name: String)
