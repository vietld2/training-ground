package com.happy.trainingground.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.happy.trainingground.view.adapter.FoodAdapter
import com.happy.trainingground.repository.FoodRepoFactory
import com.happy.trainingground.viewmodel.FoodViewModel
import com.happy.trainingground.R
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val foodViewModel =
            ViewModelProvider(
                owner = this@MainActivity,
                factory = FoodRepoFactory() // optional
            ).get(modelClass = FoodViewModel::class.java)

        val foodAdapter = FoodAdapter()
        rv_mainFood.adapter = foodAdapter

        foodViewModel.todayFoodItemList.observe(
            this@MainActivity
        ) {
            foodAdapter.submitList(it)
        }

        foodViewModel.getTodayFood(this, date = Calendar.getInstance().time)
    }


}


