package com.happy.trainingground.view.adapter

import androidx.recyclerview.widget.DiffUtil
import com.happy.trainingground.model.DataFoodItem

class FoodDiffCallBack:  DiffUtil.ItemCallback<DataFoodItem>() {
    override fun areItemsTheSame(oldItem: DataFoodItem, newItem: DataFoodItem): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: DataFoodItem, newItem: DataFoodItem): Boolean {
        return oldItem == newItem
    }
}