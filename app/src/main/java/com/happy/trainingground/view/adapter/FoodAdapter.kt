package com.happy.trainingground.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.happy.trainingground.R
import com.happy.trainingground.base.BaseListAdapter
import com.happy.trainingground.model.DataFoodItem
import kotlinx.android.synthetic.main.item_food.view.*

class FoodAdapter : BaseListAdapter<DataFoodItem>(FoodDiffCallBack()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_food, null)
        return FoodViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        holder.itemView.apply {
            tv_foodName.text = item.name
            tv_foodPrice.text = item.price.toString()
            var ingredient = ""
            item.ingredients.forEachIndexed { index, it ->
                ingredient += if (index == 0) {
                    it.name
                } else {
                    ", " + it.name
                }
            }
            tv_foodIngredient.text = ingredient.trim()
            Glide.with(this).load(item.displayImage).into(iv_foodImg)
        }
    }

    inner class FoodViewHolder(view: View) : RecyclerView.ViewHolder(view)
}