package com.happy.trainingground.repository

import android.content.Context
import com.google.gson.Gson
import com.happy.trainingground.network.FoodService
import com.happy.trainingground.model.DataFood
import com.happy.trainingground.model.DataFoodItem
import com.happy.trainingground.readFileFromAssets
import java.util.*

class FoodRepository(private val foodService: FoodService?= null) {

    companion object {
        private var instance: FoodRepository?= null
        fun getInstance(): FoodRepository {
            if(instance == null){
                instance = FoodRepository()
            }
            return instance!!
        }
    }

    fun getFoodListByDate(context: Context, date: Date): List<DataFoodItem> {
        val foodList = mutableListOf<DataFoodItem>()
//        val foodData = foodService.getTodayFood(
//          date = date.time.toString()
//        ) ==> Retrofit

        val foodData =
            context.readFileFromAssets(
                "foodSample.txt"
            ) // ==> Example

        val foodArray = Gson().fromJson(
            foodData,
            DataFood::class.java)

        foodList.addAll(foodArray)

        return foodList
    }
}

