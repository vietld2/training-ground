package com.happy.trainingground.repository

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class FoodRepoFactory() : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        try {
            return modelClass.getConstructor(FoodRepository::class.java)
                .newInstance(FoodRepository.getInstance())
        } catch (e: InstantiationException) {
            throw RuntimeException("${e.message} InstantiationException")
        } catch (e: IllegalArgumentException) {
            throw RuntimeException("${e.message} IllegalArgumentException")
        }
    }
}

