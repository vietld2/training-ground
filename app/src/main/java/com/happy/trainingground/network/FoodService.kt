package com.happy.trainingground.network


interface FoodService {
    companion object {
        private var foodService : FoodService?= null // just an example, please make it correctly
        fun getInstance(): FoodService? {
            return foodService
        }
    }
    suspend fun getTodayFood(date: String): String // make API request
}