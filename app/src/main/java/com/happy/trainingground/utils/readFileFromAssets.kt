package com.happy.trainingground

import android.content.Context
import java.io.InputStream


fun Context.readFileFromAssets(fileName: String): String {
    var data = ""
    try {
        val fin: InputStream = assets?.open(fileName)!!
        val buff = ByteArray(1024)
        var len: Int = fin.read(buff)
        while (len > 0) {
            data += String(buff, 0, len)
            len = fin.read(buff)
        }
        fin.close()
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return data
}
